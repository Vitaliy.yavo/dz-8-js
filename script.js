/* Відповідь 1: 
Це звʼязуючий інтерфес між скріптом та сторінкою WEB, що дозволяє взаємодіяти з елементами сторінки та змінювати їх вміст та властивості.*/

/* Відповідь 2: 
1)innerHTML дозволяє працювати з HTML вмістом елемента, включаючи HTML теги;
2)innerText дозволяє отримувати або змінювати тільки текстовий вміст, ігноруючи HTML теги.*/

/* Відповідь 3: 
1)getElementById: Звернення за унікальним ідентифікатором (id).
2)querySelector: Звернення за допомогою CSS-подібних селекторів (можна використовувати класи, теги, атрибути).
3)getElementsByClassName: Звернення за класом елементів (повертає колекцію елементів).
4)getElementsByTagName: Звернення за тегом елементів (повертає колекцію елементів).
5)querySelectorAll: Звернення за допомогою CSS-подібних селекторів, повертає всі збіги (повертає колекцію елементів).*/

// Задача:

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach((paragraph) => {
  paragraph.style.backgroundColor = "#ff0000";
});

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);

const childNodes = optionsList.childNodes;
childNodes.forEach((node) => {
  console.log("Назва:", node.nodeName, "Тип:", node.nodeType);
});

const testParagraph = document.getElementsByClassName("testParagraph");
testParagraph.textContent = " This is a paragraph";

const mainHeader = document.querySelector(".main-header");
const navItems = mainHeader.querySelectorAll("li");
navItems.forEach((item) => {
  item.classList.add("nav-item");
});

const sectionTitles = document.querySelectorAll(".section-title");
sectionTitles.forEach((title) => {
  title.classList.remove("section-title");
});
